package main

import (
	"log"
	"pustaka-api/book"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{})
	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	// bookRequest := book.BookRequest{
	// 	Title: "Gundam ",
	// 	Price: "58000",
	// }
	// bookService.Create(bookRequest)

	// books, err := bookRepository.FindAByID(2)
	// 	fmt.Println("Title :", book.Title)

	// db.AutoMigrate(&book.Book{})
	// bookRepository := book.NewRepository(db)
	// books, err := bookRepository.FindAll()

	// for _, book := range books {
	// 	fmt.Println("Title :", book.Title)
	// }

	//membuat data

	// book := book.Book{}
	// book.Title = "Cara Membuat Mie Instant"
	// book.Price = 90000
	// book.Discount = 10
	// book.Rating = 5
	// book.Description = "Ini adalah buku yang sangat bagus dari Mayang Yudhiastari"

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("===========================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("===========================")
	// }
	// var book book.Book

	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println("===========================")
	// 	fmt.Println("Error finding book record")
	// 	fmt.Println("===========================")
	// }

	// book.Title = "Man Tiger (Revised edition)"
	// err = db.Save(&book).Error
	// if err != nil {
	// 	fmt.Println("===========================")
	// 	fmt.Println("Error updating book record")
	// 	fmt.Println("===========================")
	// }
	// err = db.Delete(&book).Error
	// if err != nil {
	// 	fmt.Println("===========================")
	// 	fmt.Println("Error deleting book record")
	// 	fmt.Println("===========================")
	// }

	router := gin.Default()

	v1 := router.Group("/v1")

	v1.GET("/", bookHandler.RootHandler)
	v1.GET("/hello", bookHandler.HelloHandler)
	v1.GET("/books/:id/:title", bookHandler.BooksHandler)
	v1.GET("/query", bookHandler.QueryHandler)
	v1.POST("/books", bookHandler.PostBooksHandler)

	router.Run()
}
