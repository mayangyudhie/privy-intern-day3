package handler

import (
	"fmt"
	"net/http"
	"pustaka-api/book"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct {
	bookService book.Service
}
func (handler *bookHandler) RootHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"name": "Mayang Yudhiastari",
		"bio":  "A Quality Assurance Intern",
	})
}

func (handler *bookHandler) HelloHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"title":    "Hello World",
		"subtitle": "Belajar Golang di Privy",
	})
}

func (handler *bookHandler) BooksHandler(c *gin.Context) { //dinamis isinya mengikuti dengan value yang diisi pada URL
	id := c.Param("id") // Param untuk mengambil path variable
	title := c.Param("title")

	c.JSON(http.StatusOK, gin.H{"id": id, "title": title})
}

func (handler *bookHandler) QueryHandler(c *gin.Context) { //dinamis isinya mengikuti dengan value yang diisi pada URL
	title := c.Query("title") // Query String
	price := c.Query("price")

	c.JSON(http.StatusOK, gin.H{"title": title, "price": price})
}

func (handler *bookHandler) PostBooksHandler(c *gin.Context) { //dinamis isinya mengikuti dengan value yang diisi pada URL
	var bookInput book.BookRequest

	err := c.ShouldBindJSON(&bookInput)
	if err != nil {

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMessages,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"title": bookInput.Title,
		"price": bookInput.Price,
	})
}
